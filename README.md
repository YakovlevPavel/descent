# README #
* Django 2.1
* Python 3.6

## Сервисы
* src $ ./manage.py runserver

## Настройка проекта
В корне проекта создаем директорию:
* mkdir var;

В директории var создаем директории static и media:
* mkdir static;
* mkdir media;

Так же в директории var можно создать виртуальное окружение проекта:
* python3 -m venv venv

После того, как было создано виртуальное окружение, необходимо установить зависимости:
* pip install -Ur freeze.txt