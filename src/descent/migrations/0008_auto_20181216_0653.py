# Generated by Django 2.1.2 on 2018-12-16 06:53

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('descent', '0007_activationcode'),
    ]

    operations = [
        migrations.RenameField(
            model_name='activationcode',
            old_name='value',
            new_name='code',
        ),
    ]
