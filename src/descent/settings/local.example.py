from .base import *

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '6%o61j$*0@x36k%_0f-gcl#2)wnv7ngdme!s#s&86m8e(5+g1v'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []

# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases


# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'descent',
        'USER': 'descent',
        'PASSWORD': 'descent',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}
