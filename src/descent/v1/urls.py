from django.urls import path, include
from . import views

app_name = 'v1'
urlpatterns = [
    path('player/', include('descent.v1.player.urls')),
    path('activation_code/', include('descent.v1.activation_code.urls')),
    path('raffle/', include('descent.v1.raffle.urls')),
    path('result_table', views.result_table),
]
