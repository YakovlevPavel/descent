from rest_framework import serializers
from descent.models import Raffle


class RaffleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Raffle
        fields = 'id', 'description', 'name', 'url_organizer'
