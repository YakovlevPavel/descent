from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from descent.models import Raffle

from .serializers import *


@api_view(['GET'])
@permission_classes(())
def raffle(request):
    """
    get: Получение розыгрыша
    ---

    Если нет розыгрыша, то вернется пустой ответ.

    ## Параметры запроса
    -

    #### Ответ:
        {
            "description": "description",
            "id": 1,
            "name": "name",
            "url_organizer": "url_organizer"
        }


    ## Данные ответа
    * **id** - **int**: Уникальный идентификатор розыгрыша
    * **description** - **str**: Описание розыгрыша
    * **name** - **str**: Название розыгрыша
    * **url_organizer** - **int**: Ссылка на организатора розыгрыша
    """

    raffle = Raffle.objects.first()
    if raffle:
        raffle = RaffleSerializer(raffle).data
        return Response(raffle, status=status.HTTP_200_OK)

    return Response(status=status.HTTP_200_OK)
