from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response

from descent.models import User
from .player.serializers import PlayerSerializer


@api_view(['GET'])
@permission_classes(())
def result_table(request):
    """
    get: Получение результатов турнирной таблицы
    ---

    ## Параметры запроса
    -

    #### Ответ:
        {
            "result_table": [
                {
                    "id": 19,
                    "points": 40000000000,
                    "username": "player_7"
                },
                {
                    "id": 17,
                    "points": 5000000,
                    "username": "player_4"
                },
                {
                    "id": 16,
                    "points": 100000,
                    "username": "player_3"
                }
            ]
        }


    ## Данные ответа

    * **result_table** - **_obj_**: Список объектов игроков в убывающем порядке по количеству очков.
    """

    players = User.objects.filter(is_staff=False, is_superuser=False).order_by('-points')[:10]
    players = PlayerSerializer(players, many=True).data
    return Response({'result_table': players}, status.HTTP_200_OK)
