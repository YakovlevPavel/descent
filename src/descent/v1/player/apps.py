from django.apps import AppConfig


class V1PlayerConfig(AppConfig):
    name = 'v1Player'
    label = 'v1Player'
