from django.urls import path
from . import views

app_name = 'player'
urlpatterns = [
    path('registration', views.registration),
    path('authorization', views.authorization),
    path('<int:id_player>', views.accounts),
]