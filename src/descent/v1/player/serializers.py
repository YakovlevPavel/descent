from rest_framework import serializers

from descent.models import User


class ActionPlayerSerializer(serializers.Serializer):
    username = serializers.CharField(min_length=3, max_length=150)
    password = serializers.CharField(min_length=4, max_length=20)


class AuthorizationPlayerSerializer(ActionPlayerSerializer):
    pass


class CountPointsSerializer(serializers.Serializer):
    count = serializers.IntegerField(min_value=0)
    x2 = serializers.IntegerField(min_value=1)
    x3 = serializers.IntegerField(min_value=1)


class RegistrationPlayerSerializer(ActionPlayerSerializer):
    points = serializers.IntegerField(min_value=0, default=0, required=False)
    x2 = serializers.IntegerField(min_value=1, default=1000, required=False)
    x3 = serializers.IntegerField(min_value=1, default=5000, required=False)


class PlayerSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = 'id', 'username', 'points', 'x2', 'x3'
