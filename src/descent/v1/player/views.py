from django.contrib.auth.hashers import check_password, make_password
from django.db import transaction
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response

from descent import errors
from descent.models import User
from .serializers import RegistrationPlayerSerializer, PlayerSerializer, CountPointsSerializer, \
    AuthorizationPlayerSerializer


@api_view(['POST'])
@permission_classes(())
def registration(request):
    """
    post: Регистрация игрока
    ---

    ## Параметры запроса

    * **username** - **_str_**: Имя/логин игрока
    * **password** - **_str_**: Пароль игрока
     * **points** - **_int_ Необязательное поле**: Количество очков
    * **x2** - **_int_ Необязательное поле**: Коэффициент x2
    * **x3** - **_int_ Необязательное поле**: Коэффициент x3

    #### Ответ:
        {
            "id": 13,
            "points": 0,
            "username": "palyer_6",
            "x2": 1,
            "x3": 5
        }

    ## Данные ответа

    * **id** - **_int_**: Уникальный идентификатор игрока в системе
    * **points** - **_int_**: Количество очков
    * **username** - **_str_**: Имя/логин игрока
    * **x2** - **_int_**: Коэффициент x2
    * **x3** - **_int_**: Коэффициент x3
    """

    data_reg = RegistrationPlayerSerializer(data=request.data)

    if data_reg.is_valid():
        with transaction.atomic():
            if User.objects.filter(username=data_reg.validated_data['username']).exists():
                return Response({'error': errors.PLAYER_ALREADY_EXISTS}, status.HTTP_400_BAD_REQUEST)

            data_reg.validated_data['password'] = make_password(data_reg.validated_data['password'])
            player = User.objects.create(**data_reg.validated_data)

        return Response(PlayerSerializer(player).data, status.HTTP_201_CREATED)

    return Response({'error': data_reg.errors}, status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
@permission_classes(())
def authorization(request):
    """
    post: Авторизация игрока
    ---

    ## Параметры запроса

    * **username** - **_str_**: Имя/логин игрока
    * **password** - **_str_**: Пароль

    #### Ответ:

    Такой же как и в методе "Регистрация игрока"
    """

    data_auth = AuthorizationPlayerSerializer(data=request.data)

    if data_auth.is_valid():
        user_name = data_auth.validated_data['username']
        player = User.objects.filter(username=user_name)

        if not player.exists():
            return Response({'error': errors.PLAYER_NOT_FOUND}, status.HTTP_400_BAD_REQUEST)

        player = player.get()

        if not check_password(data_auth.data['password'], player.password):
            return Response({'error': errors.INVALID_PASSWORD_LOGIN}, status.HTTP_400_BAD_REQUEST)

        return Response(PlayerSerializer(player).data, status.HTTP_200_OK)

    return Response({'error': data_auth.errors}, status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PATCH'])
@permission_classes(())
def accounts(request, id_player):
    """
    get: Получение информации о игроке
    ---

    ## Параметры запроса
    В url необходимо передать id игрока

    #### Ответ:

    Такой же как и в методе "Регистрация игрока"

    patch: Увеличение количества очков
    ___

    ## Параметры запроса
    В url необходимо передать id игрока

    * **count** - **_int_**: Число, на которое необходимо увеличить очки игрока
    * **x2** - **_int_**: значение коэффициента x2;
    * **x3** - **_int_**: значение коэффициента x3;

    #### Ответ:

    Такой же как и в методе "Регистрация игрока"
    ---
    """

    player = User.objects.filter(id=id_player)

    if not player.exists():
        return Response({'error': errors.PLAYER_NOT_FOUND}, status.HTTP_404_NOT_FOUND)

    player = player.get()

    if request.method == 'PATCH':
        count_points = CountPointsSerializer(data=request.data)

        if not count_points.is_valid():
            return Response({'error': count_points.errors}, status.HTTP_400_BAD_REQUEST)

        player.points = count_points.validated_data['count']
        player.x2 = count_points.validated_data['x2']
        player.x3 = count_points.validated_data['x3']
        player.save()

    return Response(PlayerSerializer(player).data, status.HTTP_200_OK)
