from rest_framework import serializers
from descent.models import ActivationCode


class ActivationCodeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ActivationCode
        fields = 'id', 'code'
