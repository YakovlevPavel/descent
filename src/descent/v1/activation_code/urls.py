from django.urls import path
from . import views

app_name = 'activation_code'
urlpatterns = [
    path('', views.reset_value),
]
