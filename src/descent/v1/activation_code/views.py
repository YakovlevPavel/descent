from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from descent.models import ActivationCode

from .serializers import ActivationCodeSerializer


@api_view(['GET'])
@permission_classes(())
def reset_value(request):
    """
    get: Получение кода активации
    ---

    Если нет кода активации, то вернется пустой ответ.

    ## Параметры запроса
    -

    #### Ответ:
        {
            "id": 1,
            "code": "Ldladlksa",
        }

    ## Данные ответа
    * **id** - **int**: Уникальный идентификатор кода активации
    * **code** - **str**: Кад активации
    """

    activation_code = ActivationCode.objects.first()
    if activation_code:
        activation_code = ActivationCodeSerializer(activation_code).data
        return Response(activation_code, status=status.HTTP_200_OK)

    return Response(status=status.HTTP_200_OK)
