from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils import timezone


class User(AbstractUser):
    points = models.BigIntegerField(verbose_name='Количество очков', default=0)
    password = models.CharField(verbose_name='Пароль', max_length=128)
    x2 = models.IntegerField(verbose_name='x2', default=1000)
    x3 = models.IntegerField(verbose_name='x3', default=5000)


class Raffle(models.Model):
    name = models.CharField(verbose_name='Название', max_length=128)
    description = models.TextField(verbose_name='Описание', max_length=1024)
    url_organizer = models.TextField(verbose_name='Ссылка на организатора', max_length=2048)
    created_at = models.DateTimeField(verbose_name='Дата создания', default=timezone.now)

    class Meta:
        verbose_name = 'Розыгрыш'
        verbose_name_plural = 'Розыгрыши'

    def __str__(self):
        return self.name


class ActivationCode(models.Model):
    code = models.CharField(verbose_name='Код', max_length=128)

    class Meta:
        verbose_name = 'Код активации'
        verbose_name_plural = 'Коды активации'

    def __str__(self):
        return self.code
