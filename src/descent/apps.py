from django.apps import AppConfig


class Descent(AppConfig):
    name = 'descent'
    label = 'descent'
